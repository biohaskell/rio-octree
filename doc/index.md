---
title: rio-octree
---

# rio-octree

This package contains an octree implementation based on RIO. This documentation
will contain examples.

[![build](https://gitlab.com/zenhaskell/rio-octree/badges/master/build.svg)](https://gitlab.com/biohaskell/rio-octree)
[![Hackage](https://img.shields.io/hackage/v/baserock-schema.svg?maxAge=86400)](https://hackage.haskell.org/package/rio-octree)

