rio-octree
======
This is an Octree implementation using the RIO prelude.

To use simply:

```{.haskell}
module Main where

import Data.Octree as O

import Linear

main = do let oct = fromList [(V3 1 2 3, "a"),
                              (V3 3 4 5, "b"),
                              (V3 8 8 8, "c")]
              report msg elt = putStrLn $ msg ++ show elt
          report "Nearest     :" $ O.nearest     oct     $ V3 2 2 3
          report "Within range:" $ O.withinRange oct 5.0 $ V3 2 2 3
          return ()
```

Official releases are on [Hackage](http://hackage.haskell.org/package/rio-octree).
