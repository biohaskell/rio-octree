# Changelog for rio-octree

## Unreleased changes

* Fork from [Octree](http://hackage.haskell.org/package/Octree)
* Switch to hpack
* Bump to base 4.11
